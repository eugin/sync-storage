package main

import "sync-storage/client/config"

import "sync-storage/client/sessionmanager"

import "fmt"

func main() {
    config := config.Config{"/home/eugin"}
    manager := sessionmanager.New(&config)

    session := manager.CreateSession()

    getFiles := [] byte (`{"version" : 1, "id" : 4, "action" : "read_directory", "message" : { "path" : "./"}}`)

    response := session.Process(getFiles)

    fmt.Println("Response: ", string(response))

    content := [] byte (`{"version" : 1, "id" : 5, "action" : "get_files", "message" : {"files" : ["users", "producer.txt"]}}`)

    response = session.Process(content)

    fmt.Println("Response: ", string(response))

    newFile := [] byte (`{"version" : 1, "id" : 6, "action" : "add_files", "message" : {"Files":[{"Name":"lalala","Content":"Ym9vc3RtdWx0aWZydWl0CnJhaTRhZUQwCgphZmFsYWxlZXYKS3VpOHV5aXUKCnNtZWR2ZWRldgpkZWk4Y2hlSwoKenhjYXQKV2VpdjBoYWUKCmFuYXphcm92ClNvb1Q2UGhvCgptYWxhc2h0YQphUG9oa282VQoKd2Fja2VyCnlvbzJXYWl3CgpiYWdhbWFuc2hpbgpsbzV1R3VXZQoKc3ZudXNlcgpubzVJb0xvaAoKYnVpbGRlcgppZWM2b29TYQoKYWl2YW5jb3ZhCm1haXl1M0p1CgoKZGEKTWVvN29vcmUKCmFzaXRuaWtvdgphaVM1ZWVKdQo="}]}}`)

    response = session.Process(newFile)

    fmt.Println("Response: ", string(response))

    modifyFile := [] byte (`{"version" : 1, "id" : 7, "action" : "modify_files", "message" : {"Files":[{"Name":"lalala","Content":"ewogICAgICAgICAib3duZXIiOiJlb3N0cmFkZW1hcmsiLAogICAgICAgICAidG90YWxfdm90ZXMiOiIxNDI1NDAyMzU4Njc1ODQuMDkzNzUwMDAwMDAwMDAwMDAiLAogICAgICAgICAicHJvZHVjZXJfa2V5IjoiRU9TNHpUYkJYWTlTanN5WHRmUHRhQk5GdnFneHVBek1HUzllQzFYVFVwYmpwQWlWSnpncUEiLAogICAgICAgICAiaXNfYWN0aXZlIjoxLAogICAgICAgICAidXJsIjoiIiwKICAgICAgICAgInVucGFpZF9ibG9ja3MiOjAsCiAgICAgICAgICJsYXN0X2NsYWltX3RpbWUiOiIxOTcwLTAxLTAxVDAwOjAwOjAwLjAwMCIsCiAgICAgICAgICJsb2NhdGlvbiI6MAp9CiJ0b3RhbF9wcm9kdWNlcl92b3RlX3dlaWdodCI6IjQ3MzAxNDExODI1NDA4Nzk0NjI0LjAwMDAwMDAwMDAwMDAwMDAwIiwibW9yZSI6ImVvc2ZyZWVsYW5jZSIK"}]}}`)

    response = session.Process(modifyFile)

    fmt.Println("Response: ", string(response))

    deleteFile := [] byte (`{"version" : 1, "id" : 8, "action" : "delete_files", "message" : {"Files":["lalala"]}}`)

    response = session.Process(deleteFile)

    fmt.Println("Response: ", string(response))

}
