package sessionmanager

import "sync-storage/client/config"
import "sync-storage/client/session"
import "sync-storage/localstorage"


type sessionManager struct {
    sessions [] *session.Session
    config * config.Config
    storage * localstorage.Storage
}

func New(config * config.Config) * sessionManager {
    manager :=  new(sessionManager)
    manager.config = config
    manager.storage = localstorage.NewLocalStorage(config.Path)
    return manager
}

func (manager sessionManager) CreateSession () *session.Session {
    s := session.New(manager.storage)
    manager.sessions = append(manager.sessions, s)
    return s;
}

