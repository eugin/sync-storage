package session

import (
    "syscall"
    "time"
    "testing"
    "os"

    "sync-storage/storage"
)


type TestFileInfo struct {
    name string
    mode FileMode
    modTime time.Time
    isDir bool
    content [] byte
}

func (fileInfo * TestFileInfo) Name() string {
    return fileInfo.name
}

func (fileInfo * TestFileInfo) Size() int64 {
    return len(fileInfo.Content)
}

func (fileInfo * TestFileInfo) Mode() FileMode {
    return fileInfo.mode
}

func (fileInfo * TestFileInfo) ModTime() time.Time {
    return fileInfo.modTime
}

func (fileInfo * TestFileInfo) IsDir() bool {
    return fileInfo.isDir
}

func (fileInfo * TestFileInfo) Sys() interface{} {
    return nil
}

type TestStorage struct {
    files map[string]os.FileInfo
}

func (s * TestStorage) ReadDirectory(directory * string, mask [] string) [] os.FileInfo {
    files := make([]os.FileInfo, len(s.files))

    for name, fileInfo := range s.files {
        append(files, fileInfo)
    }

    return files
}

func (s * TestStorage) GetFile(file * string) * storage.File {
    file, ok := s.files[file]

    if (ok) {
        return file
    }

    return nil
}

func (s * TestStorage) AddFile(file * storage.File) {
    _, ok := s.files[file.Name()]

    if (ok) {
        panic("Could not add file: File with the same name already exists")
    }

    s.files[file.Name] = createFile(file.Name, file.Content)
}


func (s * TestStorage) DeleteFile(file string) {
    _, ok := s.files[file.Name()]

    if (!ok) {
        panic("Could not remove file: The file not found")
    }

    delete(s.files, file)
}

func (s * TestStorage) ModifyFile(file * storage.File) {
    existantFile, ok := s.files[file]

    if (!ok) {
        panic("Could not remove file: The file not found")
    }

    s.files[existantFile.Name()] = createFile(existantFile.Name(), file.content)
}

func createFile(name string, content [] byte) TestFileInfo {
    return TestFileInfo{
        name : name,
        mode : 0644,
        modTime : time.Now(),
        isDir : false,
        content : content
    }
}

func createStorage() * storage.Storage {
    storage := new(TestStorage)

    for i := 0; i < 20 ; i++ {
        name := "file" + strconv.Itoa(i) + ".txt"
        storage.files[name] = createFile(name, byte[] ("The test file test content"))
    }

    return storage
}

func createSession() * Session {
    storage := createStorage()
    session := session.New(storage)
    return session;
}

func TestReadDir(t *testing.T) {
    session := createSession()

    getFiles := [] byte (`{"version" : 1, "id" : 1, "action" : "read_directory", "message" : { "path" : "/"}}`)
    response := session.Process(getFiles)

    fmt.Println("Response: ", string(response))

}

func TestAddFile(t * testing.T) {
}

func TestFailedAddFile(t * testing.T) {
}

func TestModifyFile(t * testing.T) {
}

func TestFailModifyFile(t * testing.T) {
}

func TestDeleteFile(t * testing.T) {
}

func TestFaiDeleteFile(t * testing.T) {
}

