package session

import (
    "fmt"
    "strconv"

    "sync-storage/storage"

    "sync-storage/client/session/api"
)

type Session struct {
    storage * localstorage.Storage
}

func New(storage * localstorage.Storage) *Session {
    return &Session{storage}
}

func (s * Session) Close () {

}

func parseRequest(bytes[] byte) (*api.Request , error) {
    request, err := api.NewRequest(bytes)
    if err != nil {
        return nil, err
    }

    return request, nil
}

func (s * Session) Process (bytes [] byte) (result [] byte) {
    request, err := parseRequest(bytes)

    if err != nil {
        return [] byte("Request processing error: " + fmt.Sprintf("%v", err))
    }

    defer func(requestId int) {
        if err := recover(); err != nil {
            var errorResponse = api.Response{Id : requestId}
            errorString := fmt.Sprintf("%v", err)
            err := errorResponse.SetError(&errorString)

            if err == nil {
                result = [] byte(fmt.Sprintf("%v", err))
            }
            result = errorResponse.GetBytes()
        }
    } (request.Id)

    if request.Version != api.CurrentVersion {
        panic("Api version '" + strconv.Itoa(request.Version) + "' not supported. Current version: " + strconv.Itoa(api.CurrentVersion))
    }

    var response * api.Response

    switch request.Action {
        case "read_directory":
            response = s.readDirectory(request.ReadDirectoryMessage())
        case "get_files":
            response = s.getFiles(request.GetFilesMessage())
        case "add_files":
            response = s.addFiles(request.AddFilesMessage())
        case "delete_files":
            response = s.deleteFiles(request.DeleteFilesMessage())
        case "modify_files":
            response = s.modifyFiles(request.ModifyFilesMessage())
    }

    response.Version = api.CurrentVersion
    response.Id = request.Id
    response.Action = request.Action

    res := response.GetBytes()

    return res
}

func (s * Session) readDirectory(request * api.ReadDirectoryMessage) * api.Response {
    var response api.Response

    files := s.storage.ReadDirectory(&request.Path, request.Mask)
    response.SetDirectoryContent(files)

    return &response
}

func (s * Session) getFiles(request * api.FileNamesMessage) * api.Response {
    filesToUpload := request.Files

    files := make([] localstorage.File, len(filesToUpload))

    for i, fileName := range filesToUpload {
        files[i] = *s.storage.GetFile(&fileName)
    }

    var response api.Response
    response.SetFiles(files)

    return &response
}

func (s * Session) addFiles(request * api.FilesContentMessage) * api.Response {

    filesToSave := request.Files

    for _, file := range filesToSave {
        s.storage.AddFile(&file)
    }

    var response api.Response
    return &response
}

func (s * Session) deleteFiles(request * api.FileNamesMessage) * api.Response {
    filesToDelete := request.Files

    for _, file := range filesToDelete {
        s.storage.DeleteFile(file)
    }

    var response api.Response
    return &response
}

func (s * Session) modifyFiles(request * api.FilesContentMessage) * api.Response {
    filesToModify := request.Files

    for _, file := range filesToModify {
        s.storage.DeleteFile(file.Name)
        s.storage.AddFile(&file)
    }

    var response api.Response
    return &response
}
