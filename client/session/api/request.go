package api

import "encoding/json"
import "sync-storage/localstorage"

type Request struct {
    Version int                 `json:"version"`
    Id int                      `json:"id"`
    Action string               `json:"action"`
    Message json.RawMessage     `json:"message"`
}

func (request Request) ReadDirectoryMessage() (*ReadDirectoryMessage) {
    var readDirMessage ReadDirectoryMessage
    err := json.Unmarshal(request.Message, &readDirMessage)
    if err != nil {
        panic(err)
    }
    return &readDirMessage
}

func (request Request) GetFilesMessage() (*FileNamesMessage) {
    var getFilesMessage FileNamesMessage
    err := json.Unmarshal(request.Message, &getFilesMessage)
    if err != nil {
        panic(err)
    }
    return &getFilesMessage
}

func (request Request) AddFilesMessage() (*FilesContentMessage) {
    var addFilesMessage FilesContentMessage
    err := json.Unmarshal(request.Message, &addFilesMessage)
    if err != nil {
        panic(err)
    }
    return &addFilesMessage
}


func (request Request) DeleteFilesMessage() (*FileNamesMessage) {
    var deleteFilesMessage FileNamesMessage
    err := json.Unmarshal(request.Message, &deleteFilesMessage)
    if err != nil {
        panic(err)
    }
    return &deleteFilesMessage
}

func (request Request) ModifyFilesMessage() (*FilesContentMessage) {
    var modifyFilesMessage FilesContentMessage
    err := json.Unmarshal(request.Message, &modifyFilesMessage)
    if err != nil {
        panic(err)
    }
    return &modifyFilesMessage
}

type ReadDirectoryMessage struct {
    Path string
    Mask [] string
}

type FileNamesMessage struct {
    Files [] string
}

type FilesContentMessage struct {
    Files [] localstorage.File
}

func NewRequest(bytes[] byte) (* Request, error) {
    var request Request
    err := json.Unmarshal(bytes, &request)
    if err != nil {
        return nil, err
    }

    return &request, nil
}



