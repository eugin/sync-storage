package api

import (
    "os"
    "encoding/json"
    "sync-storage/localstorage"
)

type Response struct {
    Version int                 `json:"version"`
    Id int                      `json:"id"`
    Action string               `json:"action"`
    Message *json.RawMessage    `json:"message"`
}

func (response * Response) SetDirectoryContent(files[] os.FileInfo) {

    type fileInfo struct {
        Path string `json:"path"`
        IsDir bool  `json:"is_dir"`

    }
    message := struct {
        Files [] fileInfo `json:"files"`
    } {}

    message.Files = make([] fileInfo, len(files))

    for i, file := range files {
        message.Files[i] = fileInfo{file.Name(), file.IsDir()}
    }

    bytes, err := json.Marshal(&message)

    if err != nil {
        panic(err)
    }

    rawMessage := json.RawMessage(bytes)
    response.Message = &rawMessage
}

func (response * Response) SetFiles(files [] localstorage.File) {
  message := struct {
        Files [] localstorage.File `json:"files"`
    } {Files : files}

    bytes, err := json.Marshal(&message)

    if err != nil {
        panic(err)
    }

    rawMessage := json.RawMessage(bytes)
    response.Message = &rawMessage
}

func (response * Response) SetError(text * string) error {
    message := struct {
        Description string `json:"description"`
    } {Description : *text}

    response.Version = CurrentVersion
    response.Action = "error"

    bytes, err := json.Marshal(&message)

    if err != nil {
        return err
    }

    rawMessage := json.RawMessage(bytes)
    response.Message = &rawMessage
    return nil
}

func (response * Response) GetBytes() [] byte {
    bytes, err := json.Marshal(&response)

    if err != nil {
        return nil
    }

    return bytes
}
