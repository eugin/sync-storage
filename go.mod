module sync-storage

go 1.14

require (
	github.com/kr/fs v0.1.0
	github.com/stretchr/testify v1.6.1
)
