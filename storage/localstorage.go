package storage

import (
    "io/ioutil"
    "os"
)

type LocalStorage struct {
    root string
}

func NewLocalStorage(name * string) *LocalStorage {
    return &LocalStorage{*name}
}

func (s LocalStorage) ReadDirectory(directory * string, mask [] string) [] os.FileInfo {
    fullPath := s.getFullPath(directory)

    files, err := ioutil.ReadDir(fullPath)

    if err != nil {
        panic(err)
    }

    return files
}

func (s * LocalStorage) GetFile(file * string) *File {
    fullPath := s.getFullPath(file)
    content, err := ioutil.ReadFile(fullPath)

    if (err != nil) {
        return nil
    }

    return NewFile(*file, content)
}

func (s * LocalStorage) AddFile(file * File) {
    fullPath := s.getFullPath(&file.Name)

    if _, err := os.Stat(fullPath);  !os.IsNotExist(err) {
        panic("Could not add file: File with the same name already exists")

        return
    }

    err := ioutil.WriteFile(fullPath, file.Content, 0644) // TODO: get from app

    if (err != nil) {
        panic(err)
    }
}

func (s * LocalStorage) DeleteFile(file string) {
    fullPath := s.getFullPath(&file)

    _, err := os.Stat(fullPath)

    if os.IsNotExist(err) {
        panic("Could not remove file: The file not found")
        return
    }

    os.Remove(fullPath)
}

func (s * LocalStorage) ModifyFile(file *File) {
    fullPath := s.getFullPath(&file.Name)

    _, err := os.Stat(fullPath)

    if os.IsNotExist(err) {
        panic("Could not modify file: The file not found")
        return
    }

    os.Remove(fullPath)

    err = ioutil.WriteFile(fullPath, file.Content, 0644) // TODO: get from app

    if (err != nil) {
        panic(err)
    }
}

func (s * LocalStorage) getFullPath (path * string) string {
    return s.root + "/" + *path
}
