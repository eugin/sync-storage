package storage

import (
    "io/ioutil"
    "os"
)

type Storage interface {
    ReadDirectory(directory * string, mask [] string) [] os.FileInfo
    GetFile(file * string) *File
    AddFile(file * File)
    DeleteFile(file string)
    ModifyFile(file *File)
}
