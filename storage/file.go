package storage

type File struct {
    Name string
    Content [] byte

}

func NewFile(name string, content [] byte) *File {
    return &File{name, content}
}

