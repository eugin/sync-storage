package storage

import "testing"
import "strconv"
import "os"
import "io/ioutil"
import "github.com/stretchr/testify/assert"

const dirname string = "test_storage"
const content string = "test file content"

func createFile(name string, t *testing.T) {
    file, err := os.OpenFile(dirname + "/" + name, os.O_RDWR|os.O_CREATE, 0644)

    assert.Equal(t, err, nil, "Could not create test file")

    file.Write([] byte (content))
    file.Close()
}

func prepareDir(count int, t *testing.T) {
    os.Mkdir(dirname, 0755)

    for i :=0; i < count; i++ {
        createFile("test_file_" + strconv.Itoa(i) + ".txt", t)
    }
}

func TestReadDir(t *testing.T) {
    prepareDir(10, t)

    path := dirname
    storage := NewStorage(&path)

    root := "/"
    files := storage.ReadDirectory(&root, make([] string, 0))
    etalon, err := ioutil.ReadDir(dirname);

    assert.Equal(t, err, nil)

    assert.Equal(t, files, etalon)

    assert.Equal(t, len(files), 10)

    os.RemoveAll(dirname)
}

func TestAddFile(t *testing.T) {
    prepareDir(0, t)

    path := dirname
    storage := NewStorage(&path)

    file := File{Name : "test_file.txt", Content : [] byte(content)}

    storage.AddFile(&file)

    root := "/"

    files := storage.ReadDirectory(&root, make([] string, 0))

    assert.Equal(t, len(files), 1)

    readFile := storage.GetFile(&file.Name)

    assert.Equal(t, readFile.Content, [] byte(content))

   os.RemoveAll(dirname)
}

func TestFailedAddFile(t *testing.T) {
    prepareDir(1, t)

    path := dirname
    storage := NewStorage(&path)

    file := File{Name : "test_file_0.txt", Content : [] byte(content)}

    assert.Panics(t, func() {storage.AddFile(&file)})

   os.RemoveAll(dirname)
}

func TestModifyFile(t *testing.T) {
    prepareDir(1, t)

    path := dirname
    storage := NewStorage(&path)

    modifiedContent := "modified content"

    file := File{Name : "test_file_0.txt", Content : [] byte(modifiedContent)}

    storage.ModifyFile(&file)

    readFile := storage.GetFile(&file.Name)

    assert.Equal(t, readFile.Content, [] byte(modifiedContent))

    os.RemoveAll(dirname)
}

func TestFailModifyFile(t *testing.T) {
    prepareDir(0, t)

    path := dirname
    storage := NewStorage(&path)

    modifiedContent := "modified content"

    file := File{Name : "test_file_0.txt", Content : [] byte(modifiedContent)}

    assert.Panics(t, func() {storage.ModifyFile(&file)})

    os.RemoveAll(dirname)
}

func TestDeleteFile(t *testing.T) {
    prepareDir(1, t)

    path := dirname
    storage := NewStorage(&path)

    assert.NotPanics(t, func() {storage.DeleteFile("test_file_0.txt")})

    os.RemoveAll(dirname)
}

func TestFaiDeleteFile(t *testing.T) {
    prepareDir(0, t)

    path := dirname
    storage := NewStorage(&path)

    assert.Panics(t, func() {storage.DeleteFile("test_file_0.txt")})

    os.RemoveAll(dirname)
}
